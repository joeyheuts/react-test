##Installing the generator

* git clone this repository into a dedicated folder for your generator

* cd into the directory of the repository

* run `npm link` (use sudo if necessary)


##Using the generator

* create a new folder for your project

* run `yo reactweb`

* follow the steps in the wizard

