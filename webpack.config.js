const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')
const webpack = require('webpack')

let extractCSS = new ExtractTextPlugin('styles/[name].css');

module.exports = {
    entry: {
        app: './scripts/index.js'
    },
    devtool: 'cheap-module-source-map',
    devServer:{
      historyApiFallback: true
    },
    performance: {hints : false},
    context: path.resolve(__dirname, 'src'),
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {test: /\.scss$/i, loader: extractCSS.extract(['css-loader', 'sass-loader'])}
        ]
    },
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.[name].js'
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.bundle.js'
        }),
        extractCSS
    ],
    resolve: {
        alias: {
        MovieApi: 'src/scripts/api/movieAPI.js'
        },
        extensions: ['.js', '.scss']
    }

}
