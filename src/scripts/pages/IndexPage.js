import React from 'react'

import {Link} from 'react-router';

//components
import {ListData} from '../components/ListData'
import {GetGps} from '../components/GetGps'

class IndexPage extends React.Component {
  render(){
    return (
      <div>
        <h1>Index page</h1>
        <Link to="detail"> Go to detail page </Link>
        <ListData></ListData>
        <GetGps></GetGps>
      </div>
    )
  }
}

export default IndexPage