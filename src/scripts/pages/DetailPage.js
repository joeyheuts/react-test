import React from 'react'

import {Link} from 'react-router'

class DetailPage extends React.Component {
  render(){
    return (
      <div>
        <h1>Detail page</h1>
        <Link to="/"> Go to index page </Link>

      </div>
    )
  }
}

export default DetailPage