import React from 'react'

class GetGps extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      items: []
    }
  }
  render() {
        return (
      <div>
        loaded GetGps
      </div>
    );
  }
}

export {GetGps}

if ("geolocation" in navigator) {
  navigator.geolocation.getCurrentPosition(function(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    console.log(lat,lon);
  });
} else {
  console.log("Accepteer de GPS")
}
